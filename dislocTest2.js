var MongoClient=require('mongodb').MongoClient;
var test = require('assert');

var url="mongodb://127.0.0.1:27017/mydb";
//This version uses multiple JSON objects.

var fault = {		  
	 "faultName": "San Andreas",
	 "faultDepth": "",
	 "faultDipAngle": "",
	 "faultDipSlip": "",
	 "faultLameLambda": "",
	 "faultLameMu": "",
	 "faultLatEnd": "",
	 "faultLatStart": "",
	 "faultLength": "",
	 "faultLocationX": "",
	 "faultLocationY": "",
	 "faultLocationZ": "",
	 "faultLonEnd": "",
	 "faultLonStart": "",
	 "faultRakeAngle": "",
	 "faultStrikeAngle": "",
	 "faultStrikeSlip": "",
	 "faultTensileSlip": "",
	 "faultWidth": "",
};

var dislocProjectParams = {
	 "DEFAULT_LAT":0.0,
	 "DEFAULT_LON":0.0,
	 "gridMinXValue":-20.0,
	 "gridMinYValue":-20.0,
	 "gridXIterations":5,
	 "gridXSpacing":10.0,
	 "gridYIterations":5,
	 "gridYSpacing":10.0,
	 "observationPointStyle":1,
	 "originLat":0.0,
	 "originLon":0.0
};

var dislocProjectSummary = {
	 "DislocProjectSummary":{
		  "userName":"mpierce",
		  "projectName":"test",
		  "jobUIDStamp":"blah",
		  "creationDate":"now",
		  "kmlurl":"http://localhost/",
		  "insarKmlUrl":"http://www.google.com",
		  "azimuth":27.0,
		  "elevation":60.0,
		  "frequency":1.27,
		  "faultKmlUrl":"http://www.google.com",
		  "inputFileUrl":"http://www.google.com",
		  "outputKmlUrl":"http://www.google.com",
		  "stdoutUrl":"http://www.google.com",
		  "DislocProjectParams":dislocProjectParams,
		  "Faults":[fault]
	 }
};


MongoClient.connect(url, function(err,db){
	 test.equal(null,err);
	 test.ok(db!=null);
	 console.log("Connected correctly to server");

	 var dislocCollection="dislocProjectSummaries";
	 //This will create the collection if it doesn't exist.
	 db.createCollection(dislocCollection, function(err,result){
		  test.equal(null,err);
		  //We now have access to the colleciton.
		  db.collection(dislocCollection, {strict:true},function(err,collection) {
				test.equal(null,err);
				//This will insert the specified object into the collection.
				collection.insert(dislocProjectSummary,{w:1},function(err,result){});
				//This will find all matching results and stream these back.
				var stream=collection.find({"DislocProjectSummary.Faults.faultName":"San Andreas"}).stream();
				stream.on("data",function(item){
					 console.log(item);
				});
				stream.on("end",function(){
					 console.log("End of stream");
				});
				//This will update the matching entries.  Note we must use the $ to update all the matches.
				collection.update({"DislocProjectSummary.Faults.faultName":"San Andreas"},{$set:{"DislocProjectSummary.$.faultWidth":1.0}},{w:1,multi:true},function(err, result){
					 test.equal(null,err);
				});
		  });
		  
	 });
});
												