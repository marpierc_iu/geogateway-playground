{
	 "DislocProjectSummaryBean":{
		  "userName":"",
		  "projectName":"",
		  "jobUIDStamp":"",
		  "creationDate":"",
		  "kmlurl":"",
		  "insarKmlUrl":"",
		  "azimuth":"",
		  "elevation":"",
		  "frequency":"",
		  "faultKmlUrl":"",
		  "outputKmlUrl":"",

		  "DislocProjectParams":{
				"DEFAULT_LAT":"0.0",
				"DEFAULT_LON":"0.0",
				"gridMinXValue":"-20.0",
				"gridMinYValue":"-20.0",
				"gridXIterations":"5",
				"gridXSpacing":"10.0",
				"gridYIterations":"5",
				"gridYSpacing":"10.0",
				"observationPointStyle":"1",
				"originLat":"DEFAULT_LAT",
				"originLon":"DEFAULT_LON"
		  },
		  "DislocProjectBean":{
	 			"DislocResultsBean" :{
					 "inputFileUrl":"",
					 "jobUIDStamp":"",
					 "outputFileUrl":"",
					 "projectName":"",
					 "stdoutUrl":""
				},
				"DislocProjectParams":{
					 "DEFAULT_LAT":"0.0",
					 "DEFAULT_LON":"0.0",
					 "gridMinXValue":"-20.0",
					 "gridMinYValue":"-20.0",
					 "gridXIterations":"5",
					 "gridXSpacing":"10.0",
					 "gridYIterations":"5",
					 "gridYSpacing":"10.0",
					 "observationPointStyle":"1",
					 "originLat":"DEFAULT_LAT",
					 "originLon":"DEFAULT_LON"
				},
				"Fault":{		  
					 "faultDepth": "",
					 "faultDipAngle": "",
					 "faultDipSlip": "",
					 "faultLameLambda": "",
					 "faultLameMu": "",
					 "faultLatEnd": "",
					 "faultLatStart": "",
					 "faultLength": "",
					 "faultLocationX": "",
					 "faultLocationY": "",
					 "faultLocationZ": "",
					 "faultLonEnd": "",
					 "faultLonStart": "",
					 "faultName": "",
					 "faultRakeAngle": "",
					 "faultStrikeAngle": "",
					 "faultStrikeSlip": "",
					 "faultTensileSlip": "",
					 "faultWidth": ""
				},
				"projectName":"",
				"origin_lat":"",
				"origin_lon":""
		  }
	 }
}