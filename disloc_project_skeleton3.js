{
	 "ProjectSummary":{
		  "userName":"mpierce",
		  "applicationName":"disloc",
		  "projectName":"test",
		  "jobUIDStamp":"blah",
		  "creationDate":"now",
		  "kmlurl":"http://localhost/",
		  "insarKmlUrl":"http://www.google.com",
		  "azimuth":27.0,
		  "elevation":60.0,
		  "frequency":1.27,
		  "faultKmlUrl":"http://www.google.com",
		  "inputFileUrl":"http://www.google.com",
		  "outputKmlUrl":"http://www.google.com",
		  "stdoutUrl":"http://www.google.com",
		  
		  "DislocProjectParams":{
				"DEFAULT_LAT":"0.0",
				"DEFAULT_LON":"0.0",
				"gridMinXValue":"-20.0",
				"gridMinYValue":"-20.0",
				"gridXIterations":"5",
				"gridXSpacing":"10.0",
				"gridYIterations":"5",
				"gridYSpacing":"10.0",
				"observationPointStyle":"1",
				"originLat":0.0,
				"originLon":0.0
		  },
		  "Faults":[ {		  
				"faultName": "San Andreas",
				"faultDepth": "",
				"faultDipAngle": "",
				"faultDipSlip": "",
				"faultLameLambda": "",
				"faultLameMu": "",
				"faultLatEnd": "",
				"faultLatStart": "",
				"faultLength": "",
				"faultLocationX": "",
				"faultLocationY": "",
				"faultLocationZ": "",
				"faultLonEnd": "",
				"faultLonStart": "",
				"faultRakeAngle": "",
				"faultStrikeAngle": "",
				"faultStrikeSlip": "",
				"faultTensileSlip": "",
				"faultWidth": ""
		  }]
	 }
}