var MongoClient=require('mongodb').MongoClient;
var test = require('assert');

var url="mongodb://127.0.0.1:27017/mydb";
var fault={
	 "fault":{		  
		  "faultName": "San Andreas",
		  "faultDepth": "",
		  "faultDipAngle": "",
		  "faultDipSlip": "",
		  "faultLameLambda": "",
		  "faultLameMu": "",
		  "faultLatEnd": "",
		  "faultLatStart": "",
		  "faultLength": "",
		  "faultLocationX": "",
		  "faultLocationY": "",
		  "faultLocationZ": "",
		  "faultLonEnd": "",
		  "faultLonStart": "",
		  "faultRakeAngle": "",
		  "faultStrikeAngle": "",
		  "faultStrikeSlip": "",
		  "faultTensileSlip": "",
		  "faultWidth": "",
	 }
}

MongoClient.connect(url, function(err,db){
	 test.equal(null,err);
	 test.ok(db!=null);
	 console.log("Connected correctly to server");

	 var faultCollection="faultData";
	 db.createCollection(faultCollection, function(err,result){
		  test.equal(null,err);
		  db.collection(faultCollection, {strict:true},function(err,collection) {
				test.equal(null,err);
				collection.insert(fault,{w:1},function(err,result){});
				var stream=collection.find({"fault.faultName":"San Andreas"}).stream();
				stream.on("data",function(item){
//					 console.log(item);
				});
				stream.on("end",function(){
//					 console.log("End of stream");
				});
				collection.update({"fault.faultName":"San Andreas"},{$set:{"fault.faultWidth":"1.0"}},{w:1},function(err, result){
					 console.log(err);
					 console.log(result);
				});
		  });
		  
	 });
});
												