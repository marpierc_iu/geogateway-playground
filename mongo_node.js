var MongoClient=require('mongodb').MongoClient;
var test = require('assert');

var url="mongodb://127.0.0.1:27017/mydb";
MongoClient.connect(url, function(err,db){
	 test.equal(null,err);
	 test.ok(db!=null);
	 console.log("Connected correctly to server");

	 db.createCollection('testData', function(err,result){
		  test.equal(null,err);
		  var doc={mykey:1,fieldtoupdate:1};
		  var doc2={mykey:2, docs:[{doc1:1}]};
		  var doc3={mykey:3};
		  var collection=db.collection("testData", {strict:true},function(err,collection) {
				collection.insert([{a : 1}, {a : 2}, {a : 3}],{w:1}, function(err,result){	});
				collection.insert(doc,{w:1},function(err,result){});
				collection.update({mykey:1},{$set:{fieldtoupdate:2}},{w:1},function(err,result){});
				collection.insert(doc2,{w:1},function(err,result){});
				collection.update({mykey:2},{$push:{docs:{doc2:1}}},{w:1},function(err,result){});
				collection.insert(doc3,{w:1},function(err,result){});

				//Use this to return all items at once in an array. 
				//Not good for large matches as will use lots of memory.
				collection.find().toArray(function(err,items){
					 db.close();
				});

				//Use this to return the results in a stream. The matchhing collection
				//is returned one item at a time, where it can be processed by stream.on's callback
				//function.  
				var stream=collection.find({mykey:{$ne:2}}).stream();
				stream.on("data",function(item){
					 console.log(item);
					 console.log("Next!");
				});
				stream.on("end",function() {
					 console.log("All done");
					 db.close();

				});
				
//				collection.findOne({mykey:1},function(err,item){
//				});

		  });
	 });

});
